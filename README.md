

# Terraform
## Terraform comandos y ciclo

```terraform  init -> plan -> apply -> destroy
terraform init
terraform plan
terraform apply --auto-approve 
```

# Bucket como webHost
## copiar contenido
aws s3 --endpoint-url="http://localhost:4566" sync ./website/  "s3://static-s3-bucket-new/"
## delete all file
aws s3 --endpoint-url="http://localhost:4566" rm "s3://static-s3-bucket/" --recursive --exclude "*" --include "css/*"
aws s3 --endpoint-url="http://localhost:4566" rm "s3://static-s3-bucket/" --recursive --exclude "*" --include "*"
## list
aws s3 --endpoint-url="http://localhost:4566" ls "s3://static-s3-bucket/" --recursive
## Acces webhosts
http://localhost:4566/static-s3-bucket-new/index.html
### links
https://gist.github.com/nagelflorian/67060ffaf0e8c6016fa1050b6a4e767a


# ApiGateway
```
curl http://localhost:4566/restapis/rpi3knldsq/local/_user_request_/movies
```
{"body":"Hello from the movies API!"}%


# DynamoDB
![Configuracion en IDE](imgDoc/img.png)
![Tabla Creada](imgDoc/img_1.png)

## insert
```
aws --endpoint-url="http://localhost:4566"  dynamodb put-item \
--table-name GameScores \
--item '{
"TopScore": {"N": "23"},
"GameTitle": {"S": "Call Me Today"} ,
"UserId": {"S": "Somewhat Famous"}
}' \
--return-consumed-capacity TOTAL
```
## Querys
```
aws --endpoint-url="http://localhost:4566" dynamodb scan \
--table-name "GameScores" --return-consumed-capacity TOTAL
```

# Lambda
aws lambda get-function --function-name document-type-process-bot --endpoint-url=http://localhost:4566
aws --endpoint-url=http://localhost:4566 lambda invoke --function-name document-type-process-bot output.json

# cloudwatchlogs
aws logs describe-log-groups --endpoint-url http://localhost:4566
aws logs describe-log-streams --endpoint-url http://localhost:4566 --log-group-name /aws/lambda/document-type-process-bot
aws --endpoint=http://localhost:4566 logs get-log-events --log-group-name /aws/lambda/document-type-process-bot --log-stream-name '2022/05/30/[LATEST]95cb70f2"'


# link
- https://master.dbv83mzxb8hua.amplifyapp.com/testing-aws-without-aws-localstack

[![AWS Localstack Tutorial Video](https://img.youtube.com/vi/3_sqr0G9zb0/0.jpg)](http://www.youtube.com/watch?v=3_sqr0G9zb0)
https://www.youtube.com/watch?v=3_sqr0G9zb0&ab_channel=MostlyCode



# Api ApiGateway + Lambda
https://levelup.gitconnected.com/deploy-lambda-function-and-api-gateway-using-terraform-d12cdc50dee8
