# https://levelup.gitconnected.com/deploy-lambda-function-and-api-gateway-using-terraform-d12cdc50dee8

# Pynamodb managed policy
resource "aws_iam_policy" "iampolicy_pynamodb" {
  name        = "pynamodb-policy"
  path        = "/"
  description = "IAM policy for pynamodb usage"

  policy = file("policies/pynamoDB.json")
}



module "aws_lambda_function_rest"  {
  source = "terraform-aws-modules/lambda/aws"
  function_name = "get-person"
  description   = "My awesome lambda function"
  handler = "person.lambdaHandler"
  runtime = "nodejs12.x"
  source_path = "./lambdas/rest/"
  policies    = [aws_iam_policy.iampolicy_pynamodb.arn]

  tags = {
    Name = "get-person"
  }
}

# Testing Output
output "aws_lambda_function_rest_name" {
  value = module.aws_lambda_function_rest
}

resource "aws_api_gateway_rest_api" "apiLambda" {
  name        = "myAPI"
}



resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = aws_api_gateway_rest_api.apiLambda.id
  parent_id   = aws_api_gateway_rest_api.apiLambda.root_resource_id
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "proxyMethod" {
  rest_api_id   = aws_api_gateway_rest_api.apiLambda.id
  resource_id   = aws_api_gateway_resource.proxy.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = aws_api_gateway_rest_api.apiLambda.id
  resource_id = aws_api_gateway_method.proxyMethod.resource_id
  http_method = aws_api_gateway_method.proxyMethod.http_method

  integration_http_method = "GET"
  type                    = "AWS_PROXY"
  uri                     = module.aws_lambda_function_rest.lambda_function_invoke_arn
}

resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id   = aws_api_gateway_rest_api.apiLambda.id
  resource_id   = aws_api_gateway_rest_api.apiLambda.root_resource_id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda_root" {
  rest_api_id = aws_api_gateway_rest_api.apiLambda.id
  resource_id = aws_api_gateway_method.proxy_root.resource_id
  http_method = aws_api_gateway_method.proxy_root.http_method

  integration_http_method = "GET"
  type                    = "AWS_PROXY"
  uri                     = module.aws_lambda_function_rest.lambda_function_invoke_arn
}


resource "aws_api_gateway_deployment" "apideploy" {
  depends_on = [
    aws_api_gateway_integration.lambda,
    aws_api_gateway_integration.lambda_root,
  ]

  rest_api_id = aws_api_gateway_rest_api.apiLambda.id
  stage_name  = "test"
}


resource "aws_lambda_permission" "apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name =  module.aws_lambda_function_rest.lambda_function_name
  principal     = "apigateway.amazonaws.com"

  # The "/*/*" portion grants access from any method on any resource
  # within the API Gateway REST API.
  source_arn = "${aws_api_gateway_rest_api.apiLambda.execution_arn}/*/*"
}

# curl http://localhost:4566/restapis/vwqtucfv6r/local/_user_request_/
output "base_url" {
  value = aws_api_gateway_deployment.apideploy.invoke_url
}
