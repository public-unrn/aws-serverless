variable "aws_access_key" {
  type    = string
  default = "test"
}
variable "aws_secret_key" {
  type    = string
  default = "test"
}
variable "aws_region" {
  type    = string
  default = "us-east-1"
}


variable "website_bucket_name" {
  type    = string
  default = "static-s3-bucket-new"
}
