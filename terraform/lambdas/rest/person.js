'use strict'

var AWS = require('aws-sdk');
AWS.config.update({endpoint: "http://docker-desktop:4566"});
var dynamoDB = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});


exports.lambdaHandler = function (event, context, callback) {
    dynamoDB
        .scan({
            TableName: "GameScores",
        })
        .promise()
        .then(data => {
            var response = {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/html; charset=utf-8',
                },
                body: data,
            }
            callback(null, response)
        })
        .catch(console.error)

}

