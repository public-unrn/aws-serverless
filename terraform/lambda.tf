# aws lambda get-function --function-name document-type-process --endpoint-url=http://localhost:4566
# aws --endpoint-url=http://localhost:4566 lambda invoke --function-name document-type-process-bot output.json
module "lambda_function_bot" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = "document-type-process-bot"
  description   = "My awesome lambda function"
  handler       = "lambda_funtion.lambda_handler"
  runtime       = "python3.8"

  source_path = "./lambdas/bot/"

  environment_variables = {
    CHAT_ID                  = "406992861"
    TELEGRAM_TOKEN           = "813968666:AAH7QiFb1hjSKPvGAiMSvtikOOJmVHr9-oU"
  }

  tags = {
    Name = "document-type-process-bot"
  }
}
